# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=alligator
pkgver=0_git20200618
pkgrel=0
_commit="5d8865a8f19824625847b14c1fd49c45df5b34f6"
pkgdesc="A convergent RSS/Atom feed reader"
url="https://invent.kde.org/tfella/alligator/"
arch="all !armhf"
license="LicenseRef-KDE-Accepted-GPL"
depends="kirigami2 qt5-qtbase-sqlite"
makedepends="extra-cmake-modules qt5-qtbase-dev qt5-qtquickcontrols2-dev kcoreaddons-dev syndication-dev kconfig-dev ki18n-dev"
source="https://invent.kde.org/plasma-mobile/alligator/-/archive/$_commit/alligator-$_commit.tar.gz"
options="!check" # No tests
builddir="$srcdir/$pkgname-$_commit"

prepare() {
	default_prepare

	# qmlplugindump fails for armv7+qemu (pmb#1970). This is purely for
	# packager knowledge and doesn't affect runtime, so we can disable it.
	if [ "$CARCH" = "armv7" ]; then
		sed -i "s/ecm_find_qmlmodule/# ecm_find_qmlmodule/g" CMakeLists.txt
	fi
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="b8411f2e499e213d55d21f1fdb3208f6301dc6eadf00be761d6b671683b047c039e31e6cf23e234593d6e1109206ca86f9709a8bcc30cd6d44ba5b10bf69f02d  alligator-5d8865a8f19824625847b14c1fd49c45df5b34f6.tar.gz"
