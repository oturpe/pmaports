# Maintainer: Miles Alan <m@milesalan.com>
pkgname=sxmo-xdm
pkgver=1.1.12
pkgrel=0
pkgdesc="X Display Manager fork for Sxmo; starts with virtual keyboard and launches Sxmo UI"
options="!check" # No testsuite
url="http://xorg.freedesktop.org/"
arch="all"
license="MIT"
depends="xorg-server sessreg sxmo-utils sxmo-svkbd"
makedepends="linux-pam-dev libxmu-dev libxaw-dev libxft libxft-dev"
subpackages="$pkgname-doc"
source="
	$url/releases/individual/app/xdm-1.1.12.tar.bz2
	xdm.initd
	xdm.confd
	Xsession
	Xsetup_0
	GiveConsole
"
install="$pkgname.post-install $pkgname.pre-deinstall"
provides="xdm"

build() {
	cd xdm-$pkgver
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-ipv6 \
		--with-xdmconfigdir=/etc/X11/xdm \
		--with-pam \
		--with-xft
	make
}

package() {
	cd $srcdir/xdm-$pkgver && make DESTDIR="$pkgdir" install
	install -Dm755 "$srcdir"/xdm.initd "$pkgdir"/etc/init.d/xdm
	install -Dm644 "$srcdir"/xdm.confd "$pkgdir"/etc/conf.d/xdm
	install -Dm755 "$srcdir"/Xsession "$pkgdir"/usr/lib/X11/xdm/
	install -Dm755 "$srcdir"/GiveConsole "$pkgdir"/usr/lib/X11/xdm/
	install -Dm755 "$srcdir"/Xsetup_0 "$pkgdir"/usr/lib/X11/xdm/

	# E.g. hide the cursor for Xorg
	echo ":0 local /usr/bin/X -nocursor :0" >> "$pkgdir/etc/X11/xdm/Xservers"

	echo "xlogin*logoPadding: 20" >> "$pkgdir/etc/X11/xdm/Xresources"
	echo "xlogin*geometry: 550x300+100+400" >> "$pkgdir/etc/X11/xdm/Xresources"
	echo "xlogin*greeting: Sxmo" >> "$pkgdir/etc/X11/xdm/Xresources"
	echo "xlogin*greetFace:   Monospace:bold:size=20" >> "$pkgdir/etc/X11/xdm/Xresources"
	echo "xlogin*face:       Monospace:size=15:italic" >> "$pkgdir/etc/X11/xdm/Xresources"
	echo "xlogin*promptFace:    Monospace:size=15:bold" >> "$pkgdir/etc/X11/xdm/Xresources"
	echo "xlogin*failFace:    Monospace:size=15" >> "$pkgdir/etc/X11/xdm/Xresources"
}

sha512sums="1a4be0a070ced5db8fda6fc74794c9f9ed0cb37fa440fda6a3a7652aff62dfc3d7ba68b9facf054671ebf0f4db2a0eec29d0aa3716e3407ccd5529bac3553bdb  xdm-1.1.12.tar.bz2
24f0c8dd71db73df34b3f59709e6cfb3ef58c18b7baa0fee83eac943fc6faa2e77e1e95351dfc1687534f52c9cd84cf2033d745502745422e7fae9c2cb52f43b  xdm.initd
36a442c9d82445217a6a7eca21a1f64636bf7b9cb920bed7b24fd808a4d1e60d9a09af7024fb84218b3c9c499e7b9067e8f8e4cba23928cf9fff63139ea5960b  xdm.confd
c07672dd079bb3840ec5777c46aaa1a86aca9ad1be5193ced4fa00521dc32aeb6c50817c800863b9296c5f21399cc4805dcbf02963709db8c62ff27494ad241c  Xsession
02c45b2776a1b64a2191c870b9c80d37983feb8979c97ac94ba65e1e0556e01687ca9f463334d2d435d38ca2ff05fc1a848fd3625e16828409887037e8988b4c  Xsetup_0
eeccc1384559166972a6cff2687418423422cb4b7be8f45296db1e78aa727d290e527703ed0236e109bd3d8687c5da1ec502b7245c6228f7860c0336c7f47500  GiveConsole"
